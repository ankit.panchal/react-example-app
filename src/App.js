import logo from './logo.svg';
import './App.css';
import { GlButton, GlDropdown, GlListbox, GlProgressBar } from '@gitlab/ui';
import { VueWrapper } from 'vuera'


function App() {
  return (
    <div className="App">
      <header className="App-header">
     
     
      <VueWrapper
      component={GlButton}
      variant={"success"}
      children='Hello from Vue!'
    />


      <VueWrapper
      component={GlListbox}
      items={[{value : '123', text : '234'}, {value : '13', text : '24'} ]}
    />
     
        
      </header>
    </div>
  );
}

export default App;
